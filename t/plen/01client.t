use strict;
use warnings;

use Test::More;
use Log::Log4perl;
use Test::Exception;
Log::Log4perl->init('t/log4perl.conf');

BEGIN { use_ok('Void::Plen::Client') };

can_ok('Void::Plen::Client', 'new');
isa_ok(my $client = Void::Plen::Client->new(), 'Void::Plen::Client');
can_ok($client, 'connect');
can_ok($client, 'eventh');
can_ok($client, 'add_event');
can_ok($client, 'shutdown_server');
can_ok($client, 'db_list');
can_ok($client, 'open_db');
can_ok($client, 'port');
can_ok($client, 'host');
is($client->eventh, undef);
is($client->port, 9999);
is($client->host, '');

$client = Void::Plen::Client->new(
    port => 123,
    host => 'testhost',
    eventh => (my $ref = sub {}),
);
is($client->port, 123);
is($client->host, 'testhost');
is($client->eventh, $ref);

dies_ok { $client->add_event() } 'add_event dies with not connection';

done_testing;
