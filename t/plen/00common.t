use strict;
use warnings;

use Test::More;
use Log::Log4perl;
use Test::Exception;
Log::Log4perl->init('t/log4perl.conf');

BEGIN { use_ok('Void::Plen') };

done_testing;
