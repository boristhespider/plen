use strict;
use warnings;

use Test::More;
use Proc::Fork;
use Net::EmptyPort qw(empty_port);
use IO::Pipely qw(pipely);
use DateTime;

use Log::Log4perl;
Log::Log4perl->init('t/log4perl.conf');

use Void::Plen::Server;
use Void::Plen::Client;

my $port = empty_port(undef, 'udp');
my ($rp, $wp) = pipely();
my $c;

#XXX
system("sqlite3 t/dbs/test.sqlite 'DELETE FROM events'");

run_fork {
    child {
        my $server = Void::Plen::Server->new(port => $port, db_path => 't/dbs/');
        $server->init();
        $wp->write(1);
        $server->start();
        $wp->write(1);
        exit;
    }
};

$rp->read($c, 1);
my $client = Void::Plen::Client->new(port => $port, host => 'localhost');
$client->connect();

ok(!$client->add_event(DateTime->now(), 'Test event'));

my @dblist = $client->db_list();
ok(@dblist == 1);
is($dblist[0], 'test');

ok(!$client->open_db('unknown'));
ok($client->open_db('test'));

my $when = DateTime->now()->add(seconds => 5);
$client->add_event($when, 
    'Test event',
    'Dont forget to always do proper unit testing!',
    'Void::Plen::Event::Notify::Desktop'
);

sleep 5;
$client->shutdown_server();
$rp->read($c, 1);
done_testing;
