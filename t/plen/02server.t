use strict;
use warnings;

use Test::More;
use Log::Log4perl;
use Test::Exception;
Log::Log4perl->init('t/log4perl.conf');

BEGIN { use_ok('Void::Plen::Server') };

can_ok('Void::Plen::Server', 'new');
dies_ok {
    isa_ok(my $server = Void::Plen::Server->new(), 'Void::Plen::Server');
} 'dies with no db_path';

can_ok('Void::Plen::Server', 'new');
dies_ok {
    isa_ok(my $server = Void::Plen::Server->new(db_path => 'test_path'), 'Void::Plen::Server');
} 'dies with invalid db_path';

can_ok('Void::Plen::Server', 'db_path');
isa_ok(my $server = Void::Plen::Server->new(db_path => 't/dbs/'), 'Void::Plen::Server');

can_ok($server, 'init');
can_ok($server, 'start');
can_ok($server, 'port');
can_ok($server, 'host');
is($server->port, 9999);
is($server->host, '');

$server = Void::Plen::Server->new(
    db_path => 't/dbs',
    port    => 123,
    host    => 'testhost',
);
is($server->port, 123);
is($server->host, 'testhost');

done_testing;
