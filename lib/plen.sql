CREATE TABLE events (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    time DATETIME,
    fire TEXT,
    desc TEXT,
    active integer DEFAULT 1,
    deleted integer DEFAULT 0
);
