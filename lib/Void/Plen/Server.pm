package Void::Plen::Server;
use Moo;
use Void::Base;
use Void::Plen;
use IO::Socket::IP;
use DBI;
use Types::Path::Tiny qw(Dir);
use JSON::RPC2::Server;
use IO::Async::Listener;
use IO::Async::Loop;
use IO::Async::Socket;
use IO::Async::Timer::Absolute;
use List::Util qw(any);

has '_loop'     => (is => 'ro', default => sub { IO::Async::Loop->new });
has '_listener' => (is => 'rw');
has '_lsock'    => (is => 'rw');
has '_db'       => (is => 'rw');
has '_db_list'  => (is => 'rw', default => sub { {} });
has '_plen'     => (is => 'rw');

has '_jserv' => (
    is      => 'rw',
    lazy    => 1,
    default => sub { JSON::RPC2::Server->new() }
);

has 'db_path' => (
    is       => 'ro',
    isa      => Dir,
    required => 1,
    coerce   => Dir->coercion,
);

has 'port' => (is => 'ro', default => 9999);
has 'host' => (is => 'ro', default => '');

my $log = get_logger('Void::Plen::Server');

sub BUILD {
    my ($self) = @_;

    $log->info('Loading databases from '.$self->db_path.' ...');

    for my $db ($self->db_path->children(qr/\.sqlite$/)) {
        $log->info("... ".$db->basename);
        $self->_db_list->{$db->basename =~ s/.sqlite$//r} = $db;
    }

    $log->logcroak('No avalaible databases') if keys $self->_db_list->%* < 1;

    $self->_jserv->register_named(add_event => sub { $self->_h_add_event(@_) });
    $self->_jserv->register_named(open_db => sub { $self->_h_open_db(@_) });
    $self->_jserv->register(list_dbs => sub { $self->_h_list_dbs(@_) });
    $self->_jserv->register_named(shutdown => sub { $self->_h_shutdown(@_) });
}

sub init {
    my ($self) = @_;
    my $lsock = IO::Socket::IP->new(
        LocalPort => $self->port,
        LocalHost => $self->host,
        Proto     => 'sctp',
        Listen    => 1,
    ) or $log->logdie("$!, $@");

    my $listener = IO::Async::Listener->new(
        on_accept => sub {
            my (undef, $csock) = @_;

            $log->info('Accepted new connection from '.
                $csock->peerhost.':'.$csock->peerport.' ('.$csock->peerhostname.')'); 

            my $sock = IO::Async::Socket->new(
                handle => $csock,
                on_recv => sub {
                    my ($handle, $msg) = @_;
                    $log->debug('Recieved:'.$msg);
                    $self->_jserv->execute($msg, sub { $self->_send_response($handle, @_) });
                },
                on_recv_error => sub {
                    $log->error('recv error');
                }
            );
            $self->_loop->add($sock);
        },
    );
    $self->_listener($listener);
    $self->_loop->add($listener);
    $listener->listen(
        handle           => $lsock,
        on_resolve_error => sub { $log->logdie("Cannot resolve - $_[0]\n"); },
        on_listen_error  => sub { $log->logdie("Cannot listen\n"); },

        on_listen => sub {
            $log->info('Listening on SCTP :'.$self->port);
        }
    );
}

sub start {
    my ($self) = @_;

    $log->info('Entering main event loop');

    $self->_loop->run();

    $log->info("Exiting");
}

sub _send_response {
    my ($self, $handle, $response) = @_;
    $handle->send($response);
}

sub _plan_event {
    my ($self, $e) = @_;

    $log->info("Planning event ".$e->name." at ".$e->time."");
    my $timer = IO::Async::Timer::Absolute->new(
        time => $e->time->epoch,
        on_expire => sub { $e->notify }
    );
    $self->_loop->add($timer);
}

sub _h_add_event {
    my ($self, %params) = @_;

    if (not defined $self->_plen) {
        return (undef, 1, 'database not opened');
    }
    
    try {
        my $when = DateTime->from_epoch(epoch => $params{when});
        my $event = $self->_plen->add_event($params{name}, $params{desc}, $when, $params{fire});
        $self->_plan_event($event);
    }
    catch {
        $log->error($_);
        return (undef, 1, 'Failed to add new event');
    }
}

sub _h_list_dbs {
    my ($self) = @_;
    return [keys $self->_db_list->%*]
}

sub _h_open_db {
    my ($self, %params) = @_;

    if (!exists $self->_db_list->{$params{db}}) {
        $log->error("Request for opening invalid database: $params{db}");
        return (undef, 1, 'unkown database');
    }

    try {
        my $plen = Void::Plen->new(db_file => $self->_db_list->{$params{db}});
        $self->_plen($plen);

        for my $e ($plen->events) {
            $self->_plan_event($e);
        }
        return;
    }
    catch {
        $log->error($_);
        return (undef, 1, 'Failed to open database');
    }
}

sub _h_shutdown {
    my ($self) = @_;
    $self->_loop->stop;
}

1;
