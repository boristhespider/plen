package Void::Plen::Client;
use Moo;
use Void::Base;
use Void::Plen;
use IO::Socket::IP;
use JSON::RPC2::Client;

has '_sock' => (is => 'rw');

has '_jclient' => (
    is      => 'rw',
    lazy    => 1,
    default => sub { JSON::RPC2::Client->new() }
);

has 'port'   => (is => 'ro', default => 9999);
has 'host'   => (is => 'ro', default => '');
has 'eventh' => (is => 'rw');

my $log = get_logger('Void::Plen::Client');

sub connect {
    my ($self) = @_;

    my $sock = IO::Socket::IP->new(
        PeerPort => $self->port,
        PeerHost => $self->host,
        Proto    => 'sctp'
    ) or $log->logdie("$!, $@");
    $self->_sock($sock);
}

sub _call_method {
    my ($self, $method, %params) = @_;

    my ($jreq) = $self->_jclient->call_named($method, %params);
    $log->trace("Sending: $jreq");
    $self->_sock->send($jreq) or $log->logdie("$!, $@");

    $self->_sock->recv(my $jresp, 1024, 0) // $log->logdie("$!, $@");
    $log->trace("Recieved: $jresp");
    my ($failed, $result, $error, $call) = $self->_jclient->response($jresp);

    if ($failed) { 
        $log->error("JSON response parse error: $failed");
    }
    elsif ($error) {
        $log->error("Response error code: $error->{code}, msg: $error->{message}");
    }
    else {
        return $result // 1;
    }
    return;
}

sub _call_method_nr {
    my ($self, $method, %params) = @_;

    my ($jreq) = $self->_jclient->call_named($method, %params);
    $log->trace("Sending: $jreq");
    $self->_sock->send($jreq) or $log->logdie("$!, $@");
}

sub add_event {
    my ($self, $when, $name, $desc, @fire) = @_;

    $log->debug("Trying to add '$name' at '$when'");

    my $response = $self->_call_method(
        'add_event',
        when => $when->epoch,
        name => $name,
        desc => $desc,
        fire => \@fire,
    );
}

sub db_list {
    my ($self) = @_;
    my $result = $self->_call_method('list_dbs');

    if ($result) {
        $log->info('Recieved avalaible database list:');
        $log->info("... $_") for @$result;
        return @$result;
    }
    return;
}

sub open_db {
    my ($self, $db) = @_;
    return $self->_call_method('open_db', db => $db);
}

sub shutdown_server {
    my ($self) = @_;
    return $self->_call_method_nr('shutdown');
}

1;
