package Void::Plen::Event::Notify::Desktop;
use Moo;
use Void::Base;
use Desktop::Notify;

sub notify {
    my ($self, $event) = @_;

    my $notify = Desktop::Notify->new();
    my $notification = $notify->create(
        summary => $event->name,
        body    => '<span color="red">'.$event->desc.'</span>',
    );
    $notification->show();
}

package Void::Plen::Event;
use Moo;
use Void::Base;

has 'time' => (is => 'ro', required => 1);
has 'id'   => (is => 'ro', required => 1);
has 'name' => (is => 'ro', required => 1);
has 'desc' => (is => 'ro', required => 1);
has 'fire' => (is => 'ro', required => 1);

sub notify {
    my ($self) = @_;
    #no strict 'refs';
    for my $class ($self->fire->@*) {
        "$class"->new->notify($self);
    }
}

1;
