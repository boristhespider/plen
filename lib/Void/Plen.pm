package Void::Plen;
use Moo;
use Void::Base;
use DBI;
use Types::Path::Tiny qw(File);
use DateTime;
use Void::Plen::Event;
use JSON::XS;

my $log = get_logger('Void::Plen');

has 'db_file' => (
    is       => 'ro',
    isa      => File,
    required => 1,
    coerce   => File->coercion,
);

has '_db' => (is => 'rw');

my $EVENTS_TABLE = 'events';

sub BUILD {
    my ($self) = @_;

    my $dbh = DBI->connect("dbi:SQLite:dbname=".$self->db_file, "", "", { RaiseError => 1 });
    $self->_db($dbh);
}

sub add_event {
    my ($self, $name, $desc, $when, $fire) = @_;

    my $sth = $self->_db->prepare_cached(<<"SQLEND");
    INSERT INTO $EVENTS_TABLE(name, desc, time, fire) VALUES(?, ?, ?, ?)
SQLEND
    $sth->execute($name, $desc, $when->epoch, encode_json $fire);

    $sth = $self->_db->prepare_cached(<<"SQLEND");
    SELECT last_insert_rowid()
SQLEND
    $sth->execute();
    my $id = $sth->fetchrow_array;
    $sth->finish;

    return Void::Plen::Event->new(
        name => $name,
        desc => $desc,
        time => $when,
        id   => $id,
        fire => $fire,
    );
}

sub events {
    my ($self) = @_;

    my $sth = $self->_db->prepare_cached(<<"SQLEND");
    SELECT * FROM $EVENTS_TABLE;
SQLEND
    $sth->execute();
    my $res = $sth->fetchall_arrayref({});
    my $now = DateTime->now();
    map { 
        my $date = DateTime->from_epoch(epoch => $_->{time});
        return if $date < $now;

        Void::Plen::Event->new(
            name => $_->{name},
            time => $date,
            id   => $_->{id},
            desc => $_->{desc},
            fire => decode_json $_->{fire},
    ) } @$res;
}

1;
