package Void::Base;
use base 'Import::Base';
 
sub modules {
    my ($class, %args) = @_;
    return (
        'strict',
        'warnings',
        feature => [qw(postderef)],
        '-warnings' => [qw(experimental::postderef)],
        'Log::Log4perl' => [qw(get_logger)],
        'Try::Tiny' => [],
    );
}

1;
